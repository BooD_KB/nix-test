let
  pkgs = import <nixpkgs> {};
  app = import ./release.nix;

in
pkgs.dockerTools.buildImage {
  name = "nix-test";
  tag = "latest";
  contents = app;
  config = {
    Workingdir = "/";
    Entrypoint = [
      "/bin/nix-test"
    ];
  };
}

