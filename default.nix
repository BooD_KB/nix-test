{ mkDerivation, base, directory, stdenv }:
mkDerivation {
  pname = "nix-test";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base directory ];
  license = "unknown";
  hydraPlatforms = stdenv.lib.platforms.none;
}
