module Main
  ( main
  )
where

import           System.Environment                       ( getArgs )
import           System.Directory                         ( listDirectory )

main :: IO ()
main = do
  args <- getArgs
  let dir | [] <- args  = "."
          | [d] <- args = d
          | otherwise   = error "Too many args"
  mapM_ putStrLn =<< listDirectory dir
