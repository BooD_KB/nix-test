default: build

.PHONY: default build docker repl shell shell-pure

build:
	@nix-build release.nix

docker:
	@nix-build docker.nix
	@docker load -i ./result

repl:
	@nix-shell --pure --run "cabal repl"

shell:
	@nix-shell

shell-pure:
	@nix-shell --pure
